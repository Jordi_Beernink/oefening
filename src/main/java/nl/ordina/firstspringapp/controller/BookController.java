package nl.ordina.firstspringapp.controller;

import nl.ordina.firstspringapp.model.Book;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
// Kleine comment qua verschil
@Controller
public class BookController {
    @ResponseBody
    @RequestMapping(value = "/books", method = RequestMethod.POST)
    public Book updateBook (@RequestBody Book book){

        int year = book.getYear();
        book.setYear(year + 1);

        return book;
    }

    public Book getAllBooks() {
     System.out.println("Joe");

     Book book = new Book();
     book.setTitle("Java");
     book.setYear(2005);
     book.setAuthor("Java Guru");

     return book;
    }
}
